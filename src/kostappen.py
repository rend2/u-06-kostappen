from codecs import getencoder
from distutils.log import fatal
from tkinter import *
from tkinter import ttk
import sqlite3
from turtle import bgcolor


# root.geometry("400x400")


# Create info window function
conn = sqlite3.connect("database.db")
c = conn.cursor()


c.execute("CREATE TABLE IF NOT EXISTS profiles (age integer, weight integer, height integer, height_feet integer, height_inches integer, gender text, activity text, goal text, name text, password text, protein integer, carbs integer, fat integer)")

#Creates the information window
def new_window():
    root1 = Tk()
    root1.title("Info")
    root1.geometry("400x200")

    info_label = Label(
        root1, text="Information \n", font=14)
    info_label.grid(row=1, column=1,)
    if language.get() == "English":
        info_text = Label(
            root1, text="The diet app is a tool that helps you keep track of the amount\n \
            of food you need per day to reach your goals.\n Each unit of food corresponds to about 100 kcal.\n \
                Fill in your details and click on results to see your daily need. \n ")
    elif language.get() == "Svenska":
        info_text = Label(
            root1, text="Kostappen är ett verktyg som hjälper dig att att hålla koll på vilken\n \
            mängd livsmedel du behöver per dag för att nå dina mål.\n Varje enhet av livsmedel motsvarar ca 100 kcal.\n \
                Fyll i dina uppgifter och klicka på result för att få ditt dagsbehov. \n ")
    info_text.grid(row=3, column=1)

# Create a close button
    def close():
        root1.destroy()
    info_button = Button(root1, text="Close", command=close)
    info_button.grid(row=15, column=1, columnspan=2,
                     pady=10, padx=10, ipadx=100)

    root1.mainloop()

#Calculates and shows calories, protein, fat and carbs.
def result(name, password):
    root1 = Tk()
    root1.title("Result")
    root1.geometry("350x300")

    gender1 = gender.get()
    activity1 = activity.get()
    goal1 = goal.get()
    meals1 = meals.get()
    
    if gender1 == "Male":
        gender_value = 5
    if gender1 == "Female":
        gender_value = -161
    if gender1 == "Man":
        gender_value = 5
    if gender1 == "Kvinna":
        gender_value = -161

    if activity1 == "Low activity":
        activity_value = 400
    if activity1 == "Moderate activity":
        activity_value = 700
    if activity1 == "High activity":
        activity_value = 1000
    if activity1 == "Låg aktivitet":
        activity_value = 400
    if activity1 == "Måttlig aktivitet":
        activity_value = 700
    if activity1 == "Hög aktivitet":
        activity_value = 1000

    if goal1 == "Loose weight":
        goal_value = -500
    if goal1 == "Keep weight":
        goal_value = 0
    if goal1 == "Gain weight":
        goal_value = 500
    if goal1 == "Gå ner i vikt":
        goal_value = -500
    if goal1 == "Hålla samma vikt":
        goal_value = 0
    if goal1 == "Öka i vikt":
        goal_value = 500
        
    if meals1 == "3":
        meals_value = 3
    if meals1 == "5":
        meals_value = 5
    
    age1 = float(age.get())
    #Converts pounds to kilograms and feet/inches to centimeters
    if unit_of_measurement.get() == "Metric":
        weight1 = float(weight.get())
        height1 = float(height.get())
    elif unit_of_measurement.get() == "Imperial":
        weight1 = ((float(weight.get())) * 0.45359237)
        height1 = (((float(height_feet.get())) * 30.48) + (float(height_inches.get()) * 2.54))

    res = (weight1*10)+(height1*6.25)-(age1*5) + \
        gender_value+int(activity_value)+int(goal_value)
    
    
    round_res = round(res/100)
    

    protein = round(round_res*0.3)
    fat = round(round_res*0.3)
    carbs = round(round_res*0.4)
    if protein+fat+carbs > round_res:
        carbs -= 1
    if protein+fat+carbs < round_res:
        protein += 1
    

    pro_per_meal = round((protein / meals_value), 2)
    fat_per_meal = round((fat / meals_value), 2)
    carbs_per_meal = round((carbs / meals_value), 2)

    
    if language.get() == "Svenska":
        info_label = Label(
            root1, text="Ditt dagliga rekommenderade kaloriintag: " + str(round(res)) + " kcal", font=10)
        info_label.grid(row=1, column=1,)

        info_label1 = Label(root1, text="Detta motsvarar " +
                            str(round_res) + " enheter", font=10)
        info_label1.grid(row=2, column=1,)
        
        info_label2 = Label(root1, text="Förelningen per dag blir: \n " +
                            str(protein) + " enheter protein\n " + str(fat) + " enheter fett\n"+str(carbs)+" enheter kolhydrater\n", font=10)
        info_label2.grid(row=3, column=1)

        info_label3 = Label(root1, text="Förelningen per mål blir: \n " +
                            str(pro_per_meal) + " enheter protein\n " + str(fat_per_meal) + " enheter fett\n"+str(carbs_per_meal)+" enheter kolhydrater\n", font=10)
        info_label3.grid(row=8, column=1)
    elif language.get() == "English":
        info_label = Label(
            root1, text="Your daily recommended caloric intake: " + str(round(res)) + " kcal", font=10)
        info_label.grid(row=1, column=1,)

        info_label1 = Label(root1, text="This is equivalent to: " +
                            str(round_res) + " units", font=10)
        info_label1.grid(row=2, column=1,)
        
        info_label2 = Label(root1, text="The distribution per day is: \n " +
                            str(protein) + " units of protein\n " + str(fat) + " units of fat\n"+str(carbs)+" units of carbohydrates\n", font=10)
        info_label2.grid(row=3, column=1)

        info_label3 = Label(root1, text="The distribution per meal is: \n " +
                            str(pro_per_meal) + " units of protein\n " + str(fat_per_meal) + " units of fat\n"+str(carbs_per_meal)+" units of carbohydrates\n", font=10)
        info_label3.grid(row=8, column=1)
    
    
    #Inserts values to database based on metric or imperial unit of measurement
    if unit_of_measurement.get() == "Metric":
        with conn:
            c.execute("DELETE FROM profiles WHERE name = (?)", (name,))
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": age.get(), "weight": weight.get(), "height": height.get(), "height_feet": None, "height_inches": None, "gender": gender.get(), "activity": activity.get(), "goal": goal.get(), "name": name, "password": password, "protein": protein, "carbs": carbs, "fat": fat})
    elif unit_of_measurement.get() == "Imperial":
        with conn:
            c.execute("DELETE FROM profiles WHERE name = (?)", (name,))
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": age.get(), "weight": weight.get(), "height": None, "height_feet": height_feet.get(), "height_inches": height_inches.get(), "gender": gender.get(), "activity": activity.get(), "goal": goal.get(), "name": name, "password": password, "protein": protein, "carbs": carbs, "fat": fat})


    def close():
        root1.destroy()
        info_button = Button(root1, text="Close", command=close)
        info_button.grid(row=15, column=1, columnspan=2,
                         pady=10, padx=10, ipadx=100)


    root1.mainloop()

    print(res)
    return res

#Deletes input for metric and imperial
def delete_m():
    age.delete(0, END)
    weight.delete(0, END)
    gender.delete(0, END)
    activity.delete(0, END)
    goal.delete(0, END)
    meals.delete(0, END)
    height.delete(0, END)


def delete_i():
    age.delete(0, END)
    weight.delete(0, END)
    gender.delete(0, END)
    activity.delete(0, END)
    goal.delete(0, END)
    meals.delete(0, END)
    height_feet.delete(0, END)
    height_inches.delete(0, END)

#Selects profile
def login():
    root = Tk()
    root.title("Login")
    
    def close():
        root.destroy()


    def get_user_data(): 
        x = c.execute("SELECT * FROM profiles WHERE name = (?)", (name.get(),))
        
        for i in x:
            a = i[8]
            b = i[9]

        if name.get() == a and password.get() == b:
            y = c.execute("SELECT * FROM profiles WHERE name = (?)", (name.get(),))
            root.destroy()
            if language.get() == "English" and unit_of_measurement.get() == "Metric":
                main_screen1(y)
            elif language.get() == "English" and unit_of_measurement.get() == "Imperial":
                main_screen2(y)
            elif language.get() == "Svenska" and unit_of_measurement.get() == "Metric":
                main_screen3(y)
            elif language.get() == "Svenska" and unit_of_measurement.get() == "Imperial":
                main_screen4(y)


    if language.get() == "English":
        name = Entry(root, width=20)
        name.grid(row=0, column=1, padx=10)

        name_label = Label(root, text="Username")
        name_label.grid(row=0, column=0, padx=10)

        password = Entry(root, width=20)
        password.grid(row=1, column=1, padx=10)

        password_label = Label(root, text="Password")
        password_label.grid(row=1, column=0, padx=10)

        a = "Confirm"
        b = "Close"

    elif language.get() == "Svenska":
        name = Entry(root, width=20)
        name.grid(row=0, column=1, padx=10)

        name_label = Label(root, text="Användarnamn")
        name_label.grid(row=0, column=0, padx=10)

        password = Entry(root, width=20)
        password.grid(row=1, column=1, padx=10)

        password_label = Label(root, text="Lösenord")
        password_label.grid(row=1, column=0, padx=10)

        a = "Klar"
        b = "Stäng"
        

    Login_button = Button(root, text=(a), command=get_user_data)
    Login_button.grid(row=14, column=1, columnspan=2,
                     pady=10, padx=10, ipadx=10)
    
    close_button = Button(root, text=(b), command=close)
    close_button.grid(row=15, column=1, columnspan=2,
                     pady=10, padx=10, ipadx=10)

#Creates username and password
def create_profile():
    root = Tk()
    root.title("Create profile")
    
    conn = sqlite3.connect("database.db")
    c = conn.cursor()

    if language.get() == "English":
        name = Entry(root, width=20)
        name.grid(row=0, column=1,)

        name_label = Label(root, text="Username")
        name_label.grid(row=0, column=0, padx=10)

        password = Entry(root, width=20)
        password.grid(row=1, column=1, padx=10)

        password_label = Label(root, text="Password")
        password_label.grid(row=1, column=0)

        a = "Confirm"
        b = "Close"

    elif language.get() == "Svenska":
        name = Entry(root, width=20)
        name.grid(row=0, column=1,)

        name_label = Label(root, text="Användarnamn")
        name_label.grid(row=0, column=0, padx=10)

        password = Entry(root, width=20)
        password.grid(row=1, column=1, padx=10)

        password_label = Label(root, text="Lösenord")
        password_label.grid(row=1, column=0)

        a = "Klar"
        b = "Stäng"

    def close():
        root.destroy()


    def confirm():
        with conn:
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": None, "weight": None, "height": None, "height_feet": None, "height_inches": None, "gender": None, "activity": None, "goal": None, "name": name.get(), "password": password.get(), "protein": None, "carbs": None, "fat": None})
        root.destroy()
    

    confirm_button = Button(root, text=(a), command=confirm)
    confirm_button.grid(row=14, column=1, columnspan=2,
                     pady=10, padx=10, ipadx=10)

    close_button = Button(root, text=(b), command=close)
    close_button.grid(row=15, column=1, columnspan=2,
                     pady=10, padx=10, ipadx=10)

#English, metric
def main_screen1(profile_data):
    root = Tk()
    # root.geometry("400x400")
    root.title("Kostappen")

    # Create info window function
    for i in profile_data:
        name = i[8]
        password = i[9]

    
    global age
    global weight
    global height
    global gender
    global activity
    global goal
    global meals

    # create textboxes/comboxes
    age = Entry(root, width=4)
    age.grid(row=0, column=1,)
    weight = Entry(root, width=4)
    weight.grid(row=1, column=1,)
    height = Entry(root, width=4)
    height.grid(row=2, column=1,)
    gender = ttk.Combobox(root, width=6, values=["Male", "Female"])
    # .set gives a default value
    gender.set("Male")
    gender.grid(column=1, row=3)
    activity = ttk.Combobox(root, width=17, values=[
                        "Low activity", "Moderate activity", "High activity"])
    activity.set("Moderate activity")
    activity.grid(row=4, column=1,)
    goal = ttk.Combobox(root, width=13, values=[
                    "Loose weight", "Keep weight", "Gain weight"])
    goal.set("Keep weight")
    goal.grid(row=5, column=1,)
    meals = ttk.Combobox(root, width=13, values=[
                    "3", "5"])
    meals.set("3")
    meals.grid(row=6, column=1,)
    loggedinas_label = Label(root, text="Logged in as:")
    loggedinas_label.grid(row=7, column=0,)
    username_label = Label(root, text=name)
    username_label.grid(row=7, column=1)
    # create text box labels
    age_label = Label(root, text="Age")
    age_label.grid(row=0, column=0)
    weight_label = Label(root, text="Weight (kilograms)")
    weight_label.grid(row=1, column=0)
    height_label = Label(root, text="Height (centimeters)")
    height_label.grid(row=2, column=0)
    gender_label = Label(root, text="Gender")
    gender_label.grid(row=3, column=0)
    activity_label = Label(root, text="Activity level")
    activity_label.grid(row=4, column=0)
    goal_label = Label(root, text="Goal")
    goal_label.grid(row=5, column=0)
    meals_label = Label(root, text="Meals per day")
    meals_label.grid(row=6, column=0)

    def add_profile_to_database():
        conn = sqlite3.connect("database.db")
        c = conn.cursor()

        with conn:
            c.execute("DELETE FROM profiles WHERE name = (?)", (name,))
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": age.get(), "weight": weight.get(), "height": height.get(), "height_feet": None, "height_inches": None, "gender": gender.get(), "activity": activity.get(), "goal": goal.get(), "name": name, "password": password, "protein": None, "carbs": None, "fat": None})


    def result_name_password():
        result(name, password)


    # create a result button - def result()
    result_button = Button(root, text="Result", command=result_name_password)
    result_button.grid(row=8, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # Info button - def new_window()
    info_button = Button(root, text="Info", command=new_window,
                        bg="blue", activebackground="red")
    info_button.grid(row=9, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # create a delete button - def delete()
    delete_button = Button(root, text="Delete", command=delete_m)
    delete_button.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=100)


    # create test button from database.db
    save_button = Button(root, text="Save options", command=add_profile_to_database)
    save_button.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    
    # database.commit()
    # database.close()
    root.mainloop()

#English, imperial
def main_screen2(profile_data):
    root = Tk()
    # root.geometry("400x400")
    root.title("Kostappen")

    # Create info window function
    for i in profile_data:
        name = i[8]
        password = i[9]

    
    global age
    global weight
    global height_feet
    global height_inches
    global gender
    global activity
    global goal
    global meals

    # create textboxes/comboxes
    age = Entry(root, width=4)
    age.grid(row=0, column=1,)
    weight = Entry(root, width=4)
    weight.grid(row=1, column=1,)
    height_feet = Entry(root, width=4)
    height_inches = Entry(root, width=4)
    height_feet.grid(row=2, column=1,)
    height_inches.grid(row=2, column=2)
    gender = ttk.Combobox(root, width=6, values=["Male", "Female"])
    # .set gives a default value
    gender.set("Male")
    gender.grid(column=1, row=3)
    activity = ttk.Combobox(root, width=17, values=[
                        "Low activity", "Moderate activity", "High activity"])
    activity.set("Moderate activity")
    activity.grid(row=4, column=1,)
    goal = ttk.Combobox(root, width=13, values=[
                    "Loose weight", "Keep weight", "Gain weight"])
    goal.set("Keep weight")
    goal.grid(row=5, column=1,)
    meals = ttk.Combobox(root, width=13, values=[
                    "3", "5"])
    meals.set("3")
    meals.grid(row=6, column=1,)
    loggedinas_label = Label(root, text="Logged in as:")
    loggedinas_label.grid(row=7, column=0,)
    username_label = Label(root, text=name)
    username_label.grid(row=7, column=1)
    # create text box labels
    age_label = Label(root, text="Age")
    age_label.grid(row=0, column=0)
    weight_label = Label(root, text="Weight (lbs)")
    weight_label.grid(row=1, column=0)
    height_label = Label(root, text="Height (feet, inches)")
    height_label.grid(row=2, column=0)
    gender_label = Label(root, text="Gender")
    gender_label.grid(row=3, column=0)
    activity_label = Label(root, text="Activity level")
    activity_label.grid(row=4, column=0)
    goal_label = Label(root, text="Goal")
    goal_label.grid(row=5, column=0)
    meals_label = Label(root, text="Meals per day")
    meals_label.grid(row=6, column=0)

    def add_profile_to_database():
        conn = sqlite3.connect("database.db")
        c = conn.cursor()

        with conn:
            c.execute("DELETE FROM profiles WHERE name = (?)", (name,))
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": age.get(), "weight": weight.get(), "height": None, "height_feet": height_feet.get(), "height_inches": height_inches.get(), "gender": gender.get(), "activity": activity.get(), "goal": goal.get(), "name": name, "password": password, "protein": None, "carbs": None, "fat": None})


    def result_name_password():
        result(name, password)


    # create a result button - def result()
    result_button = Button(root, text="Result", command=result_name_password)
    result_button.grid(row=8, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # Info button - def new_window()
    info_button = Button(root, text="Info", command=new_window,
                        bg="blue", activebackground="red")
    info_button.grid(row=9, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # create a delete button - def delete()
    delete_button = Button(root, text="Delete", command=delete_i)
    delete_button.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=100)


    # create test button from database.db
    save_button = Button(root, text="Save options", command=add_profile_to_database)
    save_button.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    
    # database.commit()
    # database.close()
    root.mainloop()

#Svenska, metric
def main_screen3(profile_data):
    root = Tk()
    # root.geometry("400x400")
    root.title("Kostappen")

    # Create info window function
    for i in profile_data:
        name = i[8]
        password = i[9]

    
    global age
    global weight
    global height
    global gender
    global activity
    global goal
    global meals

    # create textboxes/comboxes
    age = Entry(root, width=4)
    age.grid(row=0, column=1,)
    weight = Entry(root, width=4)
    weight.grid(row=1, column=1,)
    height = Entry(root, width=4)
    height.grid(row=2, column=1,)
    gender = ttk.Combobox(root, width=6, values=["Man", "Kvinna"])
    # .set gives a default value
    gender.set("Man")
    gender.grid(column=1, row=3)
    activity = ttk.Combobox(root, width=17, values=[
                        "Låg aktivitet", "Måttlig aktivitet", "Hög aktivitet"])
    activity.set("Måttlig aktivitet")
    activity.grid(row=4, column=1,)
    goal = ttk.Combobox(root, width=13, values=[
                    "Gå ner i vikt", "Hålla samma vikt", "Öka i vikt"])
    goal.set("Hålla samma vikt")
    goal.grid(row=5, column=1,)
    meals = ttk.Combobox(root, width=13, values=[
                    "3", "5"])
    meals.set("3")
    meals.grid(row=6, column=1,)
    loggedinas_label = Label(root, text="Användare:")
    loggedinas_label.grid(row=7, column=0,)
    username_label = Label(root, text=name)
    username_label.grid(row=7, column=1)
    # create text box labels
    age_label = Label(root, text="Ålder")
    age_label.grid(row=0, column=0)
    weight_label = Label(root, text="Vikt (kilogram)")
    weight_label.grid(row=1, column=0)
    height_label = Label(root, text="Längd (centimeter)")
    height_label.grid(row=2, column=0)
    gender_label = Label(root, text="Kön")
    gender_label.grid(row=3, column=0)
    activity_label = Label(root, text="Aktivitets nivå")
    activity_label.grid(row=4, column=0)
    goal_label = Label(root, text="Plan")
    goal_label.grid(row=5, column=0)
    meals_label = Label(root, text="Mål om dagen")
    meals_label.grid(row=6, column=0)

    def add_profile_to_database():
        conn = sqlite3.connect("database.db")
        c = conn.cursor()

        with conn:
            c.execute("DELETE FROM profiles WHERE name = (?)", (name,))
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": age.get(), "weight": weight.get(), "height": height.get(), "height_feet": None, "height_inches": None, "gender": gender.get(), "activity": activity.get(), "goal": goal.get(), "name": name, "password": password, "protein": None, "carbs": None, "fat": None})


    def result_name_password():
        result(name, password)


    # create a result button - def result()
    result_button = Button(root, text="Resultat", command=result_name_password)
    result_button.grid(row=8, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # Info button - def new_window()
    info_button = Button(root, text="Info", command=new_window,
                        bg="blue", activebackground="red")
    info_button.grid(row=9, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # create a delete button - def delete()
    delete_button = Button(root, text="Radera", command=delete_m)
    delete_button.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=100)


    # create test button from database.db
    save_button = Button(root, text="Spara inställningar", command=add_profile_to_database)
    save_button.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    
    # database.commit()
    # database.close()
    root.mainloop()

#Svenska, imperial
def main_screen4(profile_data):
    root = Tk()
    # root.geometry("400x400")
    root.title("Kostappen")

    # Create info window function
    for i in profile_data:
        name = i[8]
        password = i[9]

    
    global age
    global weight
    global height_feet
    global height_inches
    global gender
    global activity
    global goal
    global meals

    # create textboxes/comboxes
    age = Entry(root, width=4)
    age.grid(row=0, column=1,)
    weight = Entry(root, width=4)
    weight.grid(row=1, column=1,)
    height_feet = Entry(root, width=4)
    height_inches = Entry(root, width=4)
    height_feet.grid(row=2, column=1,)
    height_inches.grid(row=2, column=2)
    gender = ttk.Combobox(root, width=6, values=["Man", "Kvinna"])
    # .set gives a default value
    gender.set("Man")
    gender.grid(column=1, row=3)
    activity = ttk.Combobox(root, width=17, values=[
                        "Låg aktivitet", "Måttlig aktivitet", "Hög aktivitet"])
    activity.set("Måttlig aktivitet")
    activity.grid(row=4, column=1,)
    goal = ttk.Combobox(root, width=13, values=[
                    "Gå ner i vikt", "Hålla samma vikt", "Öka i vikt"])
    goal.set("Hålla samma vikt")
    goal.grid(row=5, column=1,)
    meals = ttk.Combobox(root, width=13, values=[
                    "3", "5"])
    meals.set("3")
    meals.grid(row=6, column=1,)
    loggedinas_label = Label(root, text="Användare:")
    loggedinas_label.grid(row=7, column=0,)
    username_label = Label(root, text=name)
    username_label.grid(row=7, column=1)
    # create text box labels
    age_label = Label(root, text="Ålder")
    age_label.grid(row=0, column=0)
    weight_label = Label(root, text="Vikt (pund)")
    weight_label.grid(row=1, column=0)
    height_label = Label(root, text="Längd (fot, tum)")
    height_label.grid(row=2, column=0)
    gender_label = Label(root, text="Kön")
    gender_label.grid(row=3, column=0)
    activity_label = Label(root, text="Aktivitets nivå")
    activity_label.grid(row=4, column=0)
    goal_label = Label(root, text="Plan")
    goal_label.grid(row=5, column=0)
    meals_label = Label(root, text="Mål om dagen")
    meals_label.grid(row=6, column=0)

    
    def add_profile_to_database():
        conn = sqlite3.connect("database.db")
        c = conn.cursor()

        with conn:
            c.execute("DELETE FROM profiles WHERE name = (?)", (name,))
            c.execute("INSERT INTO profiles VALUES (:age, :weight, :height, :height_feet, :height_inches, :gender, :activity, :goal, :name, :password, :protein, :carbs, :fat)", {"age": age.get(), "weight": weight.get(), "height": None, "height_feet": height_feet.get(), "height_inches": height_inches.get(), "gender": gender.get(), "activity": activity.get(), "goal": goal.get(), "name": name, "password": password, "protein": None, "carbs": None, "fat": None})


    def result_name_password():
        result(name, password)


    # create a result button - def result()
    result_button = Button(root, text="Resultat", command=result_name_password)
    result_button.grid(row=8, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # Info button - def new_window()
    info_button = Button(root, text="Info", command=new_window,
                        bg="blue", activebackground="red")
    info_button.grid(row=9, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    # create a delete button - def delete()
    delete_button = Button(root, text="Radera", command=delete_i)
    delete_button.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=100)


    # create test button from database.db
    save_button = Button(root, text="Spara inställningar", command=add_profile_to_database)
    save_button.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

    
    # database.commit()
    # database.close()
    root.mainloop()

#First screen, create profile or login
root = Tk()

root.title("Kostappen")

login_button = Button(root, text="Log in/Logga in", command=login)
login_button.grid(row=1, column=0, columnspan=3, pady=10, padx=10, ipadx=100)

create_profile_button = Button(root, text="Create profile/Skapa profil", command=create_profile)
create_profile_button.grid(row=2, column=0, columnspan=3, pady=10, padx=10, ipadx=75)

language_label = Label(root, text="Language/Språk")
language_label.grid(row=3, column=0)

language = ttk.Combobox(root, width=13, values=[
                    "English", "Svenska"])
language.set("English")
language.grid(row=3, column=1,)

unit_of_measurement_label = Label(root, text="Unit of measurement/Måttenhet")
unit_of_measurement_label.grid(row=4, column=0)

unit_of_measurement = ttk.Combobox(root, width=13, values=[
                    "Metric", "Imperial"])
unit_of_measurement.set("Metric")
unit_of_measurement.grid(row=4, column=1, pady=10, padx=10)


root.mainloop()
