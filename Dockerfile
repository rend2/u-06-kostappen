FROM python:latest
WORKDIR /src
COPY . /src
CMD ["python", "./src/kostappen.py"]
